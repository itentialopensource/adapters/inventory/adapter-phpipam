## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for phpIPAM. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for phpIPAM.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the {php}IPAM. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getSections(globals, callback)</td>
    <td style="padding:15px">Returns all sections</td>
    <td style="padding:15px">{base_path}/{version}/sections/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSection(body, callback)</td>
    <td style="padding:15px">Creates new section</td>
    <td style="padding:15px">{base_path}/{version}/sections/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSection(body, callback)</td>
    <td style="padding:15px">Updates section</td>
    <td style="padding:15px">{base_path}/{version}/sections/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSection(id, globals, callback)</td>
    <td style="padding:15px">Returns specific section</td>
    <td style="padding:15px">{base_path}/{version}/sections/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSection(id, callback)</td>
    <td style="padding:15px">Deletes section with all belonging subnets and addresses</td>
    <td style="padding:15px">{base_path}/{version}/sections/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSectionSubnets(id, globals, callback)</td>
    <td style="padding:15px">Returns all subnets in section</td>
    <td style="padding:15px">{base_path}/{version}/sections/{pathv1}/subnets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSectionByName(name, globals, callback)</td>
    <td style="padding:15px">Returns specific section by name</td>
    <td style="padding:15px">{base_path}/{version}/sections/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSectionsCustomFields(globals, callback)</td>
    <td style="padding:15px">Returns custom section fields</td>
    <td style="padding:15px">{base_path}/{version}/sections/custom_fields/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnet(id, globals, callback)</td>
    <td style="padding:15px">Returns specific subnet by id</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubnet(id, callback)</td>
    <td style="padding:15px">Deletes Subnet</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetUsage(id, globals, callback)</td>
    <td style="padding:15px">Returns subnet usage</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/usage/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetFirstFree(id, globals, callback)</td>
    <td style="padding:15px">Returns first available IP address in subnet</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/first_free/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetSlaves(id, globals, callback)</td>
    <td style="padding:15px">Returns all immediate slave subnets</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/slaves/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetSlavesRecursive(id, globals, callback)</td>
    <td style="padding:15px">Returns all slave subnets recursive</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/slaves_recursive/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetAddresses(id, globals, callback)</td>
    <td style="padding:15px">Returns all addresses in subnet</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/addresses/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetIPAddress(id, ip, globals, callback)</td>
    <td style="padding:15px">Returns IP address from subnet</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/addresses/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetFirstAvailable(id, mask, globals, callback)</td>
    <td style="padding:15px">Returns first available subnet within selected for mask</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/first_subnet/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSubnetInsideSubnet(id, mask, body, callback)</td>
    <td style="padding:15px">Creates new child subnet inside subnet with specified mask</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/first_subnet/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetAllAvailable(id, mask, globals, callback)</td>
    <td style="padding:15px">Returns all available subnets within selected for mask</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/all_subnets/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetCustomFields(globals, callback)</td>
    <td style="padding:15px">Returns all subnet custom fields</td>
    <td style="padding:15px">{base_path}/{version}/subnets/custom_fields/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetCIDR(subnet, globals, callback)</td>
    <td style="padding:15px">Searches for subnet in CIDR format</td>
    <td style="padding:15px">{base_path}/{version}/subnets/cidr/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetCIDRSearch(subnet, globals, callback)</td>
    <td style="padding:15px">Searches for subnet in CIDR format</td>
    <td style="padding:15px">{base_path}/{version}/subnets/search/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSubnet(body, callback)</td>
    <td style="padding:15px">Creates new subnet</td>
    <td style="padding:15px">{base_path}/{version}/subnets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSubnet(body, callback)</td>
    <td style="padding:15px">Updates Subnet</td>
    <td style="padding:15px">{base_path}/{version}/subnets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSubnetMaskSize(id, body, callback)</td>
    <td style="padding:15px">Resizes subnet to new mask</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/resize/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSubnetSplit(id, body, callback)</td>
    <td style="padding:15px">Splits subnet to smaller subnets</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/split/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">udpateSubnetPermissions(id, body, callback)</td>
    <td style="padding:15px">Sets subnet permissions</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/permissions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubnetPermissions(id, callback)</td>
    <td style="padding:15px">Removes all permissions</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/permissions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubnetAddresses(id, callback)</td>
    <td style="padding:15px">Removes all addresses from subnet</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/truncate/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddress(id, globals, callback)</td>
    <td style="padding:15px">Returns specific address</td>
    <td style="padding:15px">{base_path}/{version}/addresses/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAddress(id, body, callback)</td>
    <td style="padding:15px">Updates address</td>
    <td style="padding:15px">{base_path}/{version}/addresses/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAddress(id, callback)</td>
    <td style="padding:15px">Deletes address</td>
    <td style="padding:15px">{base_path}/{version}/addresses/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddressStatus(id, globals, callback)</td>
    <td style="padding:15px">Checks address status</td>
    <td style="padding:15px">{base_path}/{version}/addresses/{pathv1}/ping/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddressFromSubnet(ip, subnetId, globals, callback)</td>
    <td style="padding:15px">Returns address from subnet by ip address</td>
    <td style="padding:15px">{base_path}/{version}/addresses/{pathv1}/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAddressInSubnet(ip, subnetId, callback)</td>
    <td style="padding:15px">Deletes address by IP in specific subnet</td>
    <td style="padding:15px">{base_path}/{version}/addresses/{pathv1}/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddressByIP(ip, globals, callback)</td>
    <td style="padding:15px">searches for addresses in database, returns multiple if found</td>
    <td style="padding:15px">{base_path}/{version}/addresses/search/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddressByHostname(hostname, globals, callback)</td>
    <td style="padding:15px">searches for addresses in database by hostname, returns multiple if found</td>
    <td style="padding:15px">{base_path}/{version}/addresses/search_hostname/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddressFirstFree(subnetId, globals, callback)</td>
    <td style="padding:15px">Returns first available address</td>
    <td style="padding:15px">{base_path}/{version}/addresses/first_free/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAddressFirstAvailable(subnetId, body, callback)</td>
    <td style="padding:15px">Creates new address in subnets – first available</td>
    <td style="padding:15px">{base_path}/{version}/addresses/first_free/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddressCustomFields(globals, callback)</td>
    <td style="padding:15px">Returns custom fields</td>
    <td style="padding:15px">{base_path}/{version}/addresses/custom_fields/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddressTags(globals, callback)</td>
    <td style="padding:15px">Returns all tags</td>
    <td style="padding:15px">{base_path}/{version}/addresses/tags/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpecificTag(id, globals, callback)</td>
    <td style="padding:15px">Returns specific tag</td>
    <td style="padding:15px">{base_path}/{version}/addresses/tags/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddressesByTag(id, globals, callback)</td>
    <td style="padding:15px">Returns addresses for specific tag</td>
    <td style="padding:15px">{base_path}/{version}/addresses/tags/{pathv1}/addresses/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAddress(body, callback)</td>
    <td style="padding:15px">Creates new address</td>
    <td style="padding:15px">{base_path}/{version}/addresses/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlans(globals, callback)</td>
    <td style="padding:15px">Returns all Vlans</td>
    <td style="padding:15px">{base_path}/{version}/vlan/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVlan(body, callback)</td>
    <td style="padding:15px">Creates new VLAN</td>
    <td style="padding:15px">{base_path}/{version}/vlan/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVlan(body, callback)</td>
    <td style="padding:15px">Updates VLAN</td>
    <td style="padding:15px">{base_path}/{version}/vlan/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlan(id, globals, callback)</td>
    <td style="padding:15px">Returns specific vlan</td>
    <td style="padding:15px">{base_path}/{version}/vlan/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVlan(id, body, callback)</td>
    <td style="padding:15px">Deletes VLAN</td>
    <td style="padding:15px">{base_path}/{version}/vlan/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanSubnets(id, globals, callback)</td>
    <td style="padding:15px">Returns all subnets attached to vlan</td>
    <td style="padding:15px">{base_path}/{version}/vlan/{pathv1}/subnets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanSubnetsBySection(id, sectionId, globals, callback)</td>
    <td style="padding:15px">Returns all subnets attached to vlan in specific section</td>
    <td style="padding:15px">{base_path}/{version}/vlan/{pathv1}/subnets/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanCustomFields(id, globals, callback)</td>
    <td style="padding:15px">Returns custom VLAN fields</td>
    <td style="padding:15px">{base_path}/{version}/vlan/{pathv1}/custom_fields/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanByNumber(id, number, globals, callback)</td>
    <td style="padding:15px">Searches for VLAN</td>
    <td style="padding:15px">{base_path}/{version}/vlan/{pathv1}/search/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL2Domains(globals, callback)</td>
    <td style="padding:15px">Returns all L2 domains</td>
    <td style="padding:15px">{base_path}/{version}/l2domains/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createL2Domain(body, callback)</td>
    <td style="padding:15px">Creates new L2 domain</td>
    <td style="padding:15px">{base_path}/{version}/l2domains/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateL2Domain(body, callback)</td>
    <td style="padding:15px">Updates L2 domain</td>
    <td style="padding:15px">{base_path}/{version}/l2domains/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL2Domain(id, globals, callback)</td>
    <td style="padding:15px">Returns specific L2 domain</td>
    <td style="padding:15px">{base_path}/{version}/l2domains/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL2DomainVlans(id, globals, callback)</td>
    <td style="padding:15px">Returns all VLANs within L2 domain</td>
    <td style="padding:15px">{base_path}/{version}/l2domains/{pathv1}/vlans/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL2DomainCustomFields(globals, callback)</td>
    <td style="padding:15px">Returns all custom fields</td>
    <td style="padding:15px">{base_path}/{version}/l2domains/custom_fields/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteL2Domain(id, callback)</td>
    <td style="padding:15px">Deletes L2 domain</td>
    <td style="padding:15px">{base_path}/{version}/l2domains/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVRFs(globals, callback)</td>
    <td style="padding:15px">Returns all VRFs</td>
    <td style="padding:15px">{base_path}/{version}/vrf/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVRF(body, callback)</td>
    <td style="padding:15px">Creates new VRF</td>
    <td style="padding:15px">{base_path}/{version}/vrf/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVRF(body, callback)</td>
    <td style="padding:15px">Updates VRF</td>
    <td style="padding:15px">{base_path}/{version}/vrf/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVRF(id, globals, callback)</td>
    <td style="padding:15px">Returns specific VRF</td>
    <td style="padding:15px">{base_path}/{version}/vrf/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVRF(id, callback)</td>
    <td style="padding:15px">Deletes VRF</td>
    <td style="padding:15px">{base_path}/{version}/vrf/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVRFSubnets(id, globals, callback)</td>
    <td style="padding:15px">Returns all subnets within VRF</td>
    <td style="padding:15px">{base_path}/{version}/vrf/{pathv1}/subnets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVRFCustomFields(globals, callback)</td>
    <td style="padding:15px">Returns all custom fields</td>
    <td style="padding:15px">{base_path}/{version}/vrf/custom_fields/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPhpDevices(globals, callback)</td>
    <td style="padding:15px">Returns all devices</td>
    <td style="padding:15px">{base_path}/{version}/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDevice(body, callback)</td>
    <td style="padding:15px">Creates new device</td>
    <td style="padding:15px">{base_path}/{version}/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDevice(body, callback)</td>
    <td style="padding:15px">Updates device</td>
    <td style="padding:15px">{base_path}/{version}/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPhpDevice(id, globals, callback)</td>
    <td style="padding:15px">Returns specific device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDevice(id, callback)</td>
    <td style="padding:15px">Deletes device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceSubnets(id, globals, callback)</td>
    <td style="padding:15px">Returns all subnets within device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/subnets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceAddresses(id, globals, callback)</td>
    <td style="padding:15px">Returns all addresses within device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/addresses/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesBySearchString(searchString, globals, callback)</td>
    <td style="padding:15px">Searches for devices</td>
    <td style="padding:15px">{base_path}/{version}/devices/search/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getToolsSubcontroller(subcontroller, globals, callback)</td>
    <td style="padding:15px">Returns all subcontroller objects</td>
    <td style="padding:15px">{base_path}/{version}/tools/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postToolsSubcontroller(subcontroller, body, callback)</td>
    <td style="padding:15px">Creates new subcontroller object</td>
    <td style="padding:15px">{base_path}/{version}/tools/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getToolsSubcontrollerIdentifier(subcontroller, identifier, globals, callback)</td>
    <td style="padding:15px">Returns specific subcontroller object</td>
    <td style="padding:15px">{base_path}/{version}/tools/{pathv1}/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchToolsSubcontrollerIdentifier(subcontroller, identifier, body, callback)</td>
    <td style="padding:15px">Updates subcontroller object</td>
    <td style="padding:15px">{base_path}/{version}/tools/{pathv1}/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteToolsSubcontrollerIdentifier(subcontroller, identifier, callback)</td>
    <td style="padding:15px">Deletes subcontroller object</td>
    <td style="padding:15px">{base_path}/{version}/tools/{pathv1}/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">toolsGetDeviceTypeDetails(id, callback)</td>
    <td style="padding:15px">Returns device type details</td>
    <td style="padding:15px">{base_path}/{version}/tools/device_types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">toolsGetDevicesByDeviceType(id, callback)</td>
    <td style="padding:15px">Returns all devices with devicetype</td>
    <td style="padding:15px">{base_path}/{version}/tools/device_types/{pathv1}/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">toolsGetSubnetsByVLAN(id, callback)</td>
    <td style="padding:15px">Returns all subnets that belong to VLAN</td>
    <td style="padding:15px">{base_path}/{version}/tools/vlans/{pathv1}/subnets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">toolsGetSubnetsByVRF(id, callback)</td>
    <td style="padding:15px">Returns all subnets that belong to VRF</td>
    <td style="padding:15px">{base_path}/{version}/tools/vrfs/{pathv1}/subnets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">toolsGetSubnetsByLocation(id, callback)</td>
    <td style="padding:15px">Returns all subnets that belong to Location</td>
    <td style="padding:15px">{base_path}/{version}/tools/locations/{pathv1}/subnets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">toolsGetDevicesByLocation(id, callback)</td>
    <td style="padding:15px">Returns all devices that belong to Location</td>
    <td style="padding:15px">{base_path}/{version}/tools/locations/{pathv1}/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">toolsGetRacksByLocation(id, callback)</td>
    <td style="padding:15px">Returns all racks that belong to Location</td>
    <td style="padding:15px">{base_path}/{version}/tools/locations/{pathv1}/racks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">toolsGetDevicesByRack(id, callback)</td>
    <td style="padding:15px">Returns all devices that belong to rack</td>
    <td style="padding:15px">{base_path}/{version}/tools/racks/{pathv1}/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNATDetailsAttachedObjects(id, globals, callback)</td>
    <td style="padding:15px">Returns nat details and array of all attached objects</td>
    <td style="padding:15px">{base_path}/{version}/tools/nat/{pathv1}/objects/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNATDetailsAllObjects(id, globals, callback)</td>
    <td style="padding:15px">Returns nat details and full array of all attached objects</td>
    <td style="padding:15px">{base_path}/{version}/tools/nat/{pathv1}/objects_full/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrefixCustomerType(customerType, globals, callback)</td>
    <td style="padding:15px">Returns all subnets used to deliver new subnets</td>
    <td style="padding:15px">{base_path}/{version}/prefix/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrefixCustomerTypeIpVersion(customerType, ipVersion, globals, callback)</td>
    <td style="padding:15px">Returns all subnets used to deliver new subnets for specific IP version</td>
    <td style="padding:15px">{base_path}/{version}/prefix/{pathv1}/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrefixCustomerTypeAddress(customerType, globals, callback)</td>
    <td style="padding:15px">Returns all subnets used to deliver new addresses</td>
    <td style="padding:15px">{base_path}/{version}/prefix/{pathv1}/address/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrefixCustomerTypeAddressIpVersion(customerType, ipVersion, globals, callback)</td>
    <td style="padding:15px">Returns all subnets used to deliver new addresses for specific IP version</td>
    <td style="padding:15px">{base_path}/{version}/prefix/{pathv1}/address/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrefixCustomerTypeIpVersionMask(customerType, ipVersion, mask, globals, callback)</td>
    <td style="padding:15px">Returns first available subnet for ip version and requested mask</td>
    <td style="padding:15px">{base_path}/{version}/prefix/{pathv1}/{pathv2}/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPrefixCustomerTypeIpVersionMask(customerType, ipVersion, mask, body, callback)</td>
    <td style="padding:15px">Creates first available subnet for ip version and requested mask</td>
    <td style="padding:15px">{base_path}/{version}/prefix/{pathv1}/{pathv2}/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrefixCustomerTypeIpVersionAddress(customerType, ipVersion, globals, callback)</td>
    <td style="padding:15px">Returns first available address for ip version</td>
    <td style="padding:15px">{base_path}/{version}/prefix/{pathv1}/{pathv2}/address/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPrefixCustomerTypeIpVersionAddress(customerType, ipVersion, body, callback)</td>
    <td style="padding:15px">Creates first available address for ip version</td>
    <td style="padding:15px">{base_path}/{version}/prefix/{pathv1}/{pathv2}/address/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
